/* trie.c - code for trie data structure.
 * 1) DO NOT rename this file
 * 2) DO NOT add main() to this file. Create a separate file to write the driver main function.
 * */

#include "trie.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>


/**
 * Create an empty trie
 *
 * @return Pointer to the root node.
 *
 */
trie_node *trie_create(void) {

    // TODO: Implement
}

/**
 * Insert a word in the given trie.
 *
 * @param root node of a trie
 * @param word to insert
 * @return 1 on success, 0 on failure. If the word already exists, return 1.
 */
int trie_insert(trie_node *root, char *word, unsigned int word_len) {

    // TODO: Implement
}


/**
 * Search a word in the given trie. Return 1 if word exists, otherwise return 0.
 *
 * @param root node of a trie
 * @param word Word to search in the trie
 * @param word_len Length of the word
 * @return 1 if the word exists in the trie, otherwise returns 0
 */
int trie_search(trie_node *root, char *word, unsigned int word_len) {

  // TODO: Implement

}


/**
 * Delete a word in the given trie.
 *
 * @param root node of a trie
 * @param word Word to search in the trie
 * @param word_len Length of the word
 */
void trie_delete(trie_node *root, char *word, unsigned int word_len) {

  // TODO: Implement

}
