

typedef struct node {

    // TODO: Define fields of the node

} trie_node;

// TODO: Implement the following functions in trie.c
trie_node *trie_create(void);
int trie_insert(trie_node *trie, char *word, unsigned int word_len);
int trie_search(trie_node *trie, char *word, unsigned int word_len);
void trie_delete(trie_node *trie, char *word, unsigned int word_len);
